import src

d_types = {
    "Pclass": "int8",
    "Sex": "category",
    "Age": "float16",
    "SibSp": "int8",
    "Parch": "int8",
    "Fare": "float16",
    "Embarked": "category",
    "titul": "category",
    "is_alone": "int8"
}

data_train_path = "data/raw/train.csv"
data_test_path = "data/raw/test.csv"

featured_train_path = "data/internal/train_f.csv"
featured_test_path = "data/internal/test_f.csv"

fixed_train_path = "data/internal/train_fixed.csv"
fixed_test_path = "data/internal/test_fixed.csv"

if __name__ == "__main__":
    src.add_features(data_train_path, featured_train_path)
    src.add_features(data_test_path, featured_test_path)

    src.fix_data(featured_train_path, d_types, fixed_train_path)
    src.fix_data(featured_test_path, d_types, fixed_test_path)
