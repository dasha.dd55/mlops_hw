import click

from .load_data import load_df_csv
from .save_results import save_res


def convert_types(df, t):
    return df.astype(t)

@click.command()
@click.argument('input_path', type=click.Path(exists=True))
@click.argument('output_path', type=click.Path())
def fix_data(input_path, output_path):
    """
    Function fill na values, drop columns, convert datatypes
    :param input_path: path to input data
    :param output_path: path to output data
    :return:
    """
    d_types = {
        "Pclass": "int8",
        "Sex": "category",
        "Age": "float16",
        "SibSp": "int8",
        "Parch": "int8",
        "Fare": "float16",
        "Embarked": "category",
        "titul": "category",
        "is_alone": "int8"
    }

    df = load_df_csv(input_path)
    df["Age"] = df["Age"].fillna(df["Age"].median())
    df["Fare"] = df["Fare"].fillna(df["Fare"].median())
    df = df.dropna(subset="Embarked")
    df = df.drop(["Name", "Ticket", "Cabin"], axis=1)
    df = convert_types(df, d_types)
    save_res(df, output_path)


if __name__ == "__main__":
    fix_data()