import pandas as pd


def load_df_csv(path):
    return pd.read_csv(path)
