import click
import pandas as pd


# from ..data.load_data import load_df_csv
# from ..data.save_results import save_res


def detect_titul(df):
    if "Mr." in df["Name"]:
        return "Mr"
    elif "Mrs." in df["Name"]:
        return "Mrs"
    elif "Miss." in df["Name"]:
        return "Miss"
    elif "Master." in df["Name"]:
        return "Master"
    elif "Dr." in df["Name"]:
        return "Dr"
    elif "Rev." in df["Name"]:
        return "Rev"
    else:
        return "others"


def is_alone(df):
    if df["SibSp"] == 0 and df["Parch"] == 0:
        return 1
    else:
        return 0


@click.command()
@click.argument('input_path', type=click.Path(exists=True))
@click.argument('output_path', type=click.Path())
def add_features(input_path: str, output_path: str):
    """
    Function add new features to dataframe
    :param input_path: path to input file
    :param output_path: path to output file
    """
    # df = load_df_csv(input_path)
    df = pd.read_csv(input_path)
    df = df.assign(titul=df.apply(detect_titul, axis=1))
    df = df.assign(is_alone=df.apply(is_alone, axis=1))
    df["family_size"] = df["SibSp"] + df["Parch"] + 1
    # save_res(df, output_path)
    df.to_csv(output_path, index=False)


if __name__ == "__main__":
    add_features()
