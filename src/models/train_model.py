import pandas as pd
from sklearn.ensemble import RandomForestClassifier


def train_model(df):
    X = pd.get_dummies(df.drop(['PassengerId', 'Survived'], axis=1))
    y = df['Survived']

    clf_rf = RandomForestClassifier(class_weight='balanced', random_state=RAND)

    clf_rf.fit(X, y)