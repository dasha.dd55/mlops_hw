import pandas as pd


def get_prediction(model, test_df):
    df_test = pd.get_dummies(test_df.drop(['PassengerId'], axis=1))
    pred = model.predict(df_test)

    sub = test_df[["PassengerId"]].assign(Survived=pred)
    return sub
